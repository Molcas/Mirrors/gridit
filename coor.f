******************************************************************************
*                                                                            *
* This software is copyrighted. You are granted the use of this software and *
* can redistribute it freely provided that the software is redistributed in  *
* its original form without omissions.                                       *
*                                                                            *
* Any modifications are prohibited unless explicitly permitted by the MOLCAS *
* development group.                                                         *
*                                                                            *
* Copyright: Valera Veryazov 2000-2016                                       *
*           Theoretical Chemistry                                            *
*           Lund University                                                  *
*           Sweden                                                           *
*                                                                            *
******************************************************************************
      Subroutine MyCoor(iRun,isAuto,
     &      Ox,Oy,Oz, Rx,Ry,Rz, iGx,iGy,iGz, iMagic,iCustOrig)
************************************************************************
*                                                                      *
*   Read Coordinates and calculate a cub for grid                      *
*   isAuto=1 - real job, else only print                               *
*   Origin(3) fix cub in space                                         *
*   Rx,Ry,Rz - size of cub                                             *
*   iGx,iGy,iGz - net                                                  *
*   iMagic = magic guess for net                                       *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
#include "Molcas.fh"
#include "WrkSpc.fh"
#include "grid.fh"
#include "real.fh"
#include "periodic_table.fh"
#include "wdw.fh"
      Dimension iOper(8)
      Dimension RotVec(3)
      Character*(LENIN) Byte4
      Character*4 tt
      Character*128 Line
cVV: the constant is used in all GV packages
#define R529 0.52917721067d0
*----------------------------------------------------------------------*
*     Prologue                                                         *
*----------------------------------------------------------------------*
#ifdef _DEBUG_
      Call qEnter('Coor')
#endif
*----------------------------------------------------------------------*
*     Read no.of symm. species                                         *
*----------------------------------------------------------------------*
      Call get_iScalar('nSym',nSym)
*----------------------------------------------------------------------*
*     Read symm. oper per symm. species                                *
*----------------------------------------------------------------------*
      Call Get_iArray('Symmetry operations',iOper,nSym)
*----------------------------------------------------------------------*
*     Read no. of unique atoms in the system                           *
*----------------------------------------------------------------------*
      Call Get_iScalar('Unique atoms',nAtoms)
*----------------------------------------------------------------------*
*     Read atom labels                                                 *
*----------------------------------------------------------------------*
      lw2=1
      Call Get_cArray('Unique Atom Names',AtomLbl,LENIN*nAtoms)
*----------------------------------------------------------------------*
*     Read coordinates of atoms                                        *
*----------------------------------------------------------------------*
      Call GetMem('Coor','ALLO','REAL',ipCoor,3*nSym*nAtoms)
      Call Get_dArray('Unique Coordinates',Work(ipCoor),3*nAtoms)
*----------------------------------------------------------------------*
*     Read nuclear repulsion energy                                    *
*----------------------------------------------------------------------*
*     Call Get_PotNuc(PotNuc)
c      Call Get_dScalar('PotNuc',PotNuc)
*----------------------------------------------------------------------*
*     Apply the symmetry operations                                    *
*----------------------------------------------------------------------*
      nOper=0
      If ( nSym.eq.2 ) nOper=1
      If ( nSym.eq.4 ) nOper=2
      If ( nSym.eq.8 ) nOper=3
      nCenter=nAtoms
      Do i=1,nOper
        jOper=i+1
        If ( i.eq.3 ) jOper=5
        RotVec(1)=One
        If ( IAND(iOper(jOper),1).eq.1 ) RotVec(1)=-One
        RotVec(2)=One
        If ( IAND(iOper(jOper),2).eq.2 ) RotVec(2)=-One
        RotVec(3)=One
        If ( IAND(iOper(jOper),4).eq.4 ) RotVec(3)=-One
        newAt=0
        mCenter=nCenter
        Do iAt=0,mCenter-1
          Xold=Work(ipCoor+iAt*3+0)
          Yold=Work(ipCoor+iAt*3+1)
          Zold=Work(ipCoor+iAt*3+2)
          Byte4=AtomLbl(lw2+iAt)
          Xnew=RotVec(1)*Xold
          Ynew=RotVec(2)*Yold
          Znew=RotVec(3)*Zold
          Do jAt=0,nCenter-1
             If (Byte4.eq.AtomLbl(Lw2+jAt)) Then
                Xold2=Work(ipCoor+jAt*3+0)
                Yold2=Work(ipCoor+jAt*3+1)
                Zold2=Work(ipCoor+jAt*3+2)

          If ( Xnew.eq.Xold2.and.Ynew.eq.Yold2.and.Znew.eq.Zold2)
     &                   goto 999
             Endif
            Enddo
            Work(ipCoor+nCenter*3+0)=Xnew
            Work(ipCoor+nCenter*3+1)=Ynew
            Work(ipCoor+nCenter*3+2)=Znew
            AtomLbl(lw2+nCenter)=Byte4
            nCenter=nCenter+1
            newAt=newAt+1
 999    Continue
        End Do
c        nCenter=nCenter+newAt
      End Do
* Check : are any strange names?
      NoOrig=0
      Do iAt=0,nCenter-1
       write(line,'(a)') AtomLbl(lw2+iAt)
       if(index( line,'Ori').ne.0) NoOrig=NoOrig+1
      enddo
*
      IF (ISLUSCUS .EQ. 1 .AND. ISBINARY .EQ. 3) THEN
        WRITE(LINE,'(2X,I8)') NCENTER-NOORIG
        CALL PRINTLINE(0, LINE, 10,0)
        CALL PRINTLINE(0, LINE, 0,0)
        DO IAT=0,NCENTER-1
          WRITE(LINE,'(A)') ATOMLBL(LW2+IAT)
          IF(INDEX( LINE,'ORI').EQ.0) THEN
          Byte4=ATOMLBL(LW2+IAT)(1:2)
          if(index ('0123456789',Byte4(2:2)).ne.0) Byte4(2:2)=' '
            WRITE(LINE,'(1X,A2,2X,3F15.8)') Byte4,
     &      WORK(IPCOOR+3*IAT)*x529,WORK(IPCOOR+3*IAT+1)*x529,
     &      WORK(IPCOOR+3*IAT+2)*x529
            CALL PRINTLINE(0, LINE, 50,0)
          ENDIF
        ENDDO
      END IF

      if(isBinary.eq.0) then
      if(isLine.eq.0) then
      write(LuVal,'(a,i8)') 'Natom= ',nCenter-NoOrig
      if(isUHF.eq.1) write(LuVal_ab,'(a,i8)') 'Natom= ',nCenter-NoOrig
      Do iAt=0,nCenter-1
       write(line,'(a)') AtomLbl(lw2+iAt)

       if(index( line,'Ori').eq.0) then
        Write(LuVal,'(A,2X,3F15.8)') AtomLbl(lw2+iAt),
     &  Work(ipCoor+3*iAt),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt+2)
       if(isUHF.eq.1)then
        Write(LuVal_ab,'(A,2X,3F15.8)') AtomLbl(lw2+iAt),
     &  Work(ipCoor+3*iAt),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt+2)
       endif

       end if
      enddo
      else
       nGatoms=nCenter-NoOrig
       call molcas_open(37,'coord.gtmp')
c       open(37,file='coord.gtmp')
       do iip=1,Num_Elem
       call upcase(PTab(iip))
       enddo
      Do iAt=0,nCenter-1
       write(Byte4,'(a)') AtomLbl(lw2+iAt)
c Well, let's draw He in a worst case
       ip=2
       call upcase(Byte4)
        do ii=1,4
        if(index ('0123456789',Byte4(ii:ii)).ne.0) Byte4(ii:ii)=' '
        enddo
        if(Byte4(2:2).eq.' ') then
        Byte4(2:2)=Byte4(1:1)
        Byte4(1:1)=' '
        endif
       do iip=1,Num_Elem
       if(Byte4.eq.PTab(iip)) then
         ip=iip
       endif
       enddo

c ... jochen: it seems that the coordinates in the write statement
c           below are reversed. I noticed a problem with
c           generated cube files
c       if(index( Byte4,'Ori').eq.0)
c     & write(37,'(I5,4F12.6)')
c     &  ip,0.0,
c     &  Work(ipCoor+3*iAt+2),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt)
       if(index( Byte4,'Ori').eq.0)
     & write(37,'(I5,4F12.6)')
     &  ip,0.0,
     &  Work(ipCoor+3*iAt),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt+2)
c ... jochen end

      enddo
      close(37)
      endif
      endif
      if(isBinary.eq.1) then
      write(Line,'(a,i8)') 'Natom= ',nCenter-NoOrig
      write(LuVal) Line(1:15)
      if(isUHF.eq.1) write(LuVal_ab) Line(1:15)
      Do iAt=0,nCenter-1
       write(line,'(a)') AtomLbl(lw2+iAt)
       if(index( line,'Ori').eq.0)
     & Write(line,'(A,2X,3F15.8)')
     &  AtomLbl(lw2+iAt),
     &  Work(ipCoor+3*iAt),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt+2)
      write(LuVal) trim(Line)
      if(isUHF.eq.1) write(LuVal_ab) trim(Line)
      enddo
      endif
      if(iCustOrig.eq.1) goto 500

      if(isAuto.eq.1) Then
*----------------------------------------------------------------------*
*     Find Cub parameters                                              *
*                 Ox->RxMin, Rx->RxMax
*----------------------------------------------------------------------*
      Ox=9999.9D0
      Oy=9999.9D0
      Oz=9999.9D0
      Rx=-9999.9D0
      Ry=-9999.9D0
      Rz=-9999.9D0

      Do iAt=0,nCenter-1
       rrx=Work(ipCoor+3*iAt)
       rry=Work(ipCoor+3*iAt+1)
       rrz=Work(ipCoor+3*iAt+2)
       if(rrx.lt.Ox) Ox=rrx
       if(rrx.gt.Rx) Rx=rrx
       if(rry.lt.Oy) Oy=rry
       if(rry.gt.Ry) Ry=rry
       if(rrz.lt.Oz) Oz=rrz
       if(rrz.gt.Rz) Rz=rrz
      End Do
       Rx=Rx-Ox
       Ry=Ry-Oy
       Rz=Rz-Oz
*
* and now, expand this cub to place all atoms inside
*
      Ox=dble(int(Ox-TheGap))
      Oy=dble(int(Oy-TheGap))
      Oz=dble(int(Oz-TheGap))
      Rx=dble(int(Rx+2.0d0*TheGap))
      Ry=dble(int(Ry+2.0d0*TheGap))
      Rz=dble(int(Rz+2.0d0*TheGap))
* finish of iAuto.
      endif
*----------------------------------------------------------------------*
*     Calculate corrected coords
*----------------------------------------------------------------------*
*
* make a stupid Patch: Cerius2 works well only with even nets!
*
       Rx=dble(int(Rx)/2*2)
       Ry=dble(int(Ry)/2*2)
       Rz=dble(int(Rz)/2*2)

       if(iMagic.gt.0) then
           iGx=int(abs(Rx))*iMagic
           iGy=int(abs(Ry))*iMagic
           iGz=int(abs(Rz))*iMagic
        endif
*
* make a stupid Patch: Cerius2 works well only with even nets!
*
      iGx=(iGx+1)/2*2
      iGy=(iGy+1)/2*2
      iGz=(iGz+1)/2*2
      mynCenter=nCenter

*----------------------------------------------------------------------*
*     Print coordinates of the system                                  *
*----------------------------------------------------------------------*
c      call bXML('Coord')
c      call iXML('nCoord',nCenter)
c      Do iAt=0,nCenter-1
c        Call cXML('Atom',AtomLbl(lw2+iAt))
c        call daXML('Atom coord',Work(ipCoor+3*iAt),3)
c      End Do
c      call eXML('Coord')
      isWDW=1
      call getmem('WDW','ALLO','REAL',ipWdW,nCenter)
      Do iAt=0,nCenter-1
        Byte4=AtomLbl(lw2+iAt)
       call upcase(Byte4)
        do ii=1,4
        if(index ('0123456789',Byte4(ii:ii)).ne.0) Byte4(ii:ii)=' '
        enddo
        if(Byte4(2:2).eq.' ') then
        Byte4(2:2)=Byte4(1:1)
        Byte4(1:1)=' '
        endif
        do j=1,103
          tt=PTab(j)
          call upcase(tt)
          if(Byte4 .eq. tt) Work(ipWdW+iAt)=WdWr(j)
        enddo
c      print *,'here',Byte4,Work(ipWdW+iAt)
      enddo


500   if(iRun.eq.1.and.levelprint.ge.3) then
      Write(6,*)
      Write(6,'(6X,A)')'Cartesian coordinates:'
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,'(6X,A)')'No.  Label     X         Y         Z     '
      Write(6,'(6X,A)')'-----------------------------------------'
      Do iAt=0,nCenter-1
        Write(6,'(4X,I4,3X,A,2X,3F10.5)')
     &  iAt+1,AtomLbl(lw2+iAt),
     &  Work(ipCoor+3*iAt),Work(ipCoor+3*iAt+1),Work(ipCoor+3*iAt+2)
      End Do
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,'(6X,A,3F12.6)')'Grid Origin      = ',Ox,Oy,Oz
      Write(6,'(6X,A,3F12.6)')'Grid Axis Length = ',Rx,Ry,Rz
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,*)
      Write(6,*)
      endif
c       if (isAtom.eq.0)
c     &   Call GetMem('Coor','FREE','REAL',ipCoor,3*nSym*nAtoms)
*
*----------------------------------------------------------------------*
*     Normal exit                                                      *
*----------------------------------------------------------------------*
#ifdef _DEBUG_
      Call qExit('Coor')
#endif
      Return
      End
*----------------------------------------------------------------------*
      Subroutine Sphr_Grid(Coords,mCoor,Coor,SphrDist,SphrColor)
      Implicit Real*8 (A-H,O-Z)
      Real*8 Coords(3,*)
      Real*8 Coor(3,mCoor), SphrDist(mCoor),SphrColor(mCoor)
#include "Molcas.fh"
#include "WrkSpc.fh"
#include "grid.fh"
#include "real.fh"

      nCenter=mynCenter

      do i=1,mCoor
      x=Coor(1,i)
      y=Coor(2,i)
      z=Coor(3,i)
c        f=10000.0d0
        f=0.0d0
        do j=1,nCenter
          xa=Coords(1,j)
          ya=Coords(2,j)
          za=Coords(3,j)
          wdwa=Work(ipWdW+j-1)
          ra=(x-xa)**2+(y-ya)**2+(z-za)**2
          ra=sqrt(ra)/(wdwa)
          f=f+exp(-ra)
        enddo
      SphrDist(i)=log(f)
      SphrColor(i)=(x**2+y**2+z**2)/(abs(x)+abs(y)+abs(z))**2
      enddo
      return
      end
