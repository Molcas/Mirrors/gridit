      subroutine Grid_driver(iForce,ThisMod,INPORB,ireturn)
c
c Make a guess: do we need to run Grid_it or not?
c  1. if iForce is set to 0 or 1 - by keyword input
c  2. if iForce set to -1: check is MOLCAS_GRID set to Yes or No?
c  3. if MOLCAS_GRID is not set - use LASTMOD (a guess from auto -
c      what is the last module.
c
      Character INPORB*(*)
      Character ThisMod*(*)
      Character Label*64
      ireturn=-1
      if(iForce.eq.0) return
      if(iForce.eq.1) goto 800
c
      Label=' '
      call getenvf("MOLCAS_GRID",Label)
      if(Label(1:1).eq.'Y'.or.Label(1:1).eq.'y') goto 800
      if(Label(1:1).eq.'N'.or.Label(1:1).eq.'n') return
      Label=' '
      call getenvf("MOLCAS_LASTMOD",Label)
      if(ThisMod.eq.Label) goto 800
      return
800   Call Grid_it(0,INPORB,ireturn)
      return
      end
