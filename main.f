      program main
#ifdef _FPE_TRAP_
      Use, Intrinsic :: IEEE_Exceptions
#endif
      implicit real*8 (a-h,o-z)
      Character*20 Module_Name
      Parameter (Module_Name = 'grid_it')
      Character*256 INPORB
#ifdef _FPE_TRAP_
      Call IEEE_Set_Halting_Mode(IEEE_Usual,.True._4)
#endif
      Call Start(Module_Name)
      INPORB='INPORB'
      Call grid_it(1,INPORB,ireturn)
      Call Finish(ireturn)
      end
