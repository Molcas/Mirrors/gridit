******************************************************************************
*                                                                            *
* This software is copyrighted. You are granted the use of this software and *
* can redistribute it freely provided that the software is redistributed in  *
* its original form without omissions.                                       *
*                                                                            *
* Any modifications are prohibited unless explicitly permitted by the MOLCAS *
* development group.                                                         *
*                                                                            *
* Copyright: Valera Veryazov 2000-2016                                       *
*           Theoretical Chemistry                                            *
*           Lund University                                                  *
*           Sweden                                                           *
*                                                                            *
******************************************************************************


C FIXME!!! The whole algorithm should be changed to smth better!

      Subroutine PackBlock(xMoBlock,iPackedBlock,mCoor,xLimits,iYDelta)
      Implicit Real*8 (A-H,O-Z)
#include "itmax.fh"
#include "info.fh"
c#include "print.fh"


c#include <SysDef.fh>
#include "grid.fh"

      Dimension xMoBlock(mCoor)
      Dimension iPackedBlock(mCoor)
      Dimension xLimits(4)
      Integer iYDelta(3)

      Dimension xDelta(3)
      Integer iyLimits(4)
      Logical minus

c DEBUG:
#ifdef _DEBUG_
      write (6,*) 'Debug: xHiErr, xLoErr, yRange:',
     *    xHiErr, xLoErr, iyRange
#endif
c Find minimum and maximum values
      xmax=abs(xMoBlock(1))
      xmin=xmax

      Do i=2,mCoor
        x=abs(xMoBlock(i))
        if (xmin .gt. x) xmin=x
        if (xmax .lt. x) xmax=x
      End Do

      xLimits(1)=xmin
      xLimits(2)=XLeft
      xLimits(3)=XRight
      xLimits(4)=xmax
#ifdef _DEBUG_
      write (6,*) 'DEBUG: xlimits:',(xLimits(i), i=1,4)
#endif
      ileft=1
      iright=3

C FIXME!: incorrect floating point comparison
c avoid empty interval
      if (xmin .eq. xmax) then
        xmax=xmin+.1
      endif

C FIXME!: incorrect floating point comparison
      if (xLimits(2) .le. xmin) then
        xLimits(2)=xmin
        ileft=2
        iYDelta(1)=0
      else
        iYDelta(1)=5
      endif

C FIXME!: incorrect floating point comparison
      if (xLimits(3) .ge. xmax) then
        xLimits(3)=xmax
        iright=2
        iYDelta(3)=0
      else
c        iYDelta(3)=int((xLimits(4)/xLimits(3)-1)/xHiErr)
        iYDelta(3)=5
      endif

C FIXME!: incorrect floating point comparison
c Avoid reversed interval
      if (xLimits(3) .le. xLimits(2)) then
        xLimits(2)=xmin
        ileft=2
        iYDelta(1)=0
        xLimits(3)=xmax
        iright=2
        iYDelta(3)=0
      endif

c REAL to avoid overflow!
      ydelta2=(xLimits(3)/xLimits(2)-1)/xLoErr
#ifdef _DEBUG_
      write (6,*) 'DEBUG: ydelta2:',ydelta2
#endif
      if (ydelta2 .gt. iYRange) then
        iYDelta(2)=iYRange
      else
        iYDelta(2)=Nint(ydelta2)
      endif

c Desirable iYDelta are set up, try to fit into iYRange
#ifdef _DEBUG_
      write (6,*) 'DEBUG: corrected xlimits:',(xLimits(i), i=1,4)
      write (6,*) 'DEBUG: uncorrected ydelta:',(iYDelta(i), i=1,3)
#endif

      if (iYDelta(1)+iYDelta(2)+iYDelta(3) .gt. iYRange) then
        iYDelta(2)=iYRange-(iYDelta(1)+iYDelta(3))
        goto 100
      endif

      if (ileft .eq. 1) then
        iYDelta(1)=int((1-xLimits(1)/xLimits(2))/xHiErr)
        if (iYDelta(1)+iYDelta(2)+iYDelta(3) .gt. iYRange) then
          iYDelta(1)=iYRange-(iYDelta(2)+iYDelta(3))
          goto 100
        endif
        if (iYDelta(1).lt.5) iYDelta(1)=5
      endif

      if (iright .eq. 3) then
        iYDelta(3)=int((xLimits(4)/xLimits(3)-1)/xHiErr)
        if (iYDelta(1)+iYDelta(2)+iYDelta(3) .gt. iYRange) then
          iYDelta(3)=iYRange-(iYDelta(1)+iYDelta(2))
          goto 100
        endif
        if (iYDelta(3).lt.5) iYDelta(3)=5
      endif

#ifdef _DEBUG_
      write (6,*) 'DEBUG: unscaled ydelta:',(iYDelta(i), i=1,3)
#endif
      scale=Dble(iYRange)/Dble(iYDelta(1)+iYDelta(2)+iYDelta(3))
#ifdef _DEBUG_
      write (6,*) 'DEBUG: scale:',scale
#endif
      iYDelta(1)=int(iYDelta(1)*scale)
      iYDelta(3)=int(iYDelta(3)*scale)
      iYDelta(2)=iYRange-(iYDelta(1)+iYDelta(3))

100   continue

c Now, iYDelta are adjusted
c DEBUG:
#ifdef _DEBUG_
      write (6,*) 'DEBUG: iYDelta:',(iYDelta(i), i=1,3)
      write (6,*) 'DEBUG: iLeft,iRight:', ileft,iright
#endif
      iyLimits(1)=0
      iyLimits(2)=iYDelta(1)
      iyLimits(3)=iyLimits(2)+iYDelta(2)
      iyLimits(4)=iyLimits(3)+iYDelta(3)

c DEBUG:
#ifdef _DEBUG_
      write (6,*) 'DEBUG: iYLimits:',(iyLimits(i), i=1,4)
#endif
      Do i=ileft,iright
        xDelta(i)=xLimits(i+1)-xLimits(i)
      End Do

Cc Re-adjust x_max -- to avoid y==y_max
C      xLimits(iright+1)=xLimits(iright+1)+
C     +   xDelta(iright)/(iYDelta(iright)-1)
C      xDelta(iright)=xLimits(iright+1)-xLimits(iright)
C      write (*,*) 'DEBUG: readjusted xlimits:',(xLimits(i), i=1,4)

      Do j=1,mCoor
c Easy to notice initial value for Y -- for debugging
        y=123
        x=xMoBlock(j)
        minus=.false.
        if (x .lt. 0) then
         minus=.true.
         x=-x
        endif

        if (x .lt. xLimits(1)) then
          y=iyLimits(1)
          goto 200
        endif

        if (x .ge. xLimits(4)) then
          y=iyLimits(4)
          goto 200
        endif

        Do i=ileft,iright
          if ((x .ge. xLimits(i)) .and. (x .lt. xLimits(i+1))) then
            y=(x-xLimits(i))/xDelta(i)*iYDelta(i)+iyLimits(i)
            goto 200
          endif
        End Do

200     continue
        if (minus) y=-y
        if (y .eq. iyRange) y=y-1
        iPackedBlock(j)=iyRange+int(y)
      End Do

      End

**********************************************************************
**********************************************************************

      Subroutine IArrToChar(iArr,cArr,len)
      Implicit Real*8 (A-H,O-Z)
#include "itmax.fh"
#include "info.fh"
c#include "print.fh"


c#include <SysDef.fh>
#include "grid.fh"

      Dimension iArr(*)
      Character cArr(*)

      idst=1
      do isrc=1,len
        if (nBytesPackedVal.eq.1) then
          CArr(idst)=char(IArr(isrc))
          idst=idst+1
        else
          ihi=IArr(isrc)/256
          ilo=IArr(isrc)-ihi*256
c Storing Most Significant First !!!
          CArr(idst)=char(ihi)
          idst=idst+1
          CArr(idst)=char(ilo)
          idst=idst+1
        endif
      enddo

      End
