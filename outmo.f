******************************************************************************
*                                                                            *
* This software is copyrighted. You are granted the use of this software and *
* can redistribute it freely provided that the software is redistributed in  *
* its original form without omissions.                                       *
*                                                                            *
* Any modifications are prohibited unless explicitly permitted by the MOLCAS *
* development group.                                                         *
*                                                                            *
* Copyright: Valera Veryazov 2000-2016                                       *
*           Theoretical Chemistry                                            *
*           Lund University                                                  *
*           Sweden                                                           *
*                                                                            *
******************************************************************************

c  ... Create output (cout) as some combination of MOs ...
c    imo>0 means use that MO only (otherwise the linear combination
c      specified by clincomb is used).
c    ipower=1 use MO, ipower=2 use MO squared, ipower=-2 use MO squared
c      keeping the phase unchanged.
      subroutine outmo(imo,ipower,cmo,clincomb,cout,nbas,nmo)
      implicit real*8 (a-h,o-z)
      dimension cmo(nbas,nmo),clincomb(nmo),cout(nbas)
#include "WrkSpc.fh"

      if(imo.ne.0)then
         call fmove(cmo(1,imo),cout,nbas)
         call power(cout,nbas,ipower)
         return
      else
        call fzero(cout,nbas)
        Call GetMem('TmpMo','Allo','Real',ipTmpMo,nbas)
        do 100 i=1,nmo
        if(clincomb(i).ne.0d0)then
          call fmove(cmo(1,i),Work(ipTmpMo),nbas)
          call power(Work(ipTmpMO),nbas,ipower)
          call daxpy_(nbas,clincomb(i),work(ipTmpMo),1,cout,1)
        endif
100     continue
        Call GetMem('TmpMo','Free','Real',ipTmpMo,nbas)
      endif
      return
      end

      subroutine curdensmo(imo,cmoR,cmoI,clincomb,
     &                     coutR,coutI,nbas,nmo)
      implicit real*8 (a-h,o-z)
      dimension cmoR(4,nbas,nmo),cmoI(4,nbas,nmo),clincomb(nmo),
     &          coutR(4,nbas),coutI(4,nbas)
#include "WrkSpc.fh"

      call fzero(coutR,4*nbas)
      call fzero(coutI,4*nbas)
c      write(6,*) "In curdensmo",iDir

      if(imo.ne.0)then

        do i=1,nbas
        do iD=1,3

c Don't forget we are multiplying by a factor of i
c Left part
          coutI(iD,i)=coutI(iD,i)+(
     &             cmoR(1,i,imo)*cmoR(iD+1,i,imo)
     &            +cmoI(1,i,imo)*cmoI(iD+1,i,imo))
          coutR(iD,i)=coutR(iD,i)+(
     &             cmoR(1,i,imo)*cmoI(iD+1,i,imo)
     &            -cmoI(1,i,imo)*cmoR(iD+1,i,imo))
c Right part
          coutI(iD,i)=coutI(iD,i)-(
     &             cmoR(iD+1,i,imo)*cmoR(1,i,imo)
     &            +cmoI(iD+1,i,imo)*cmoI(1,i,imo))
          coutR(iD,i)=coutR(iD,i)-(
     &             cmoR(iD+1,i,imo)*cmoI(1,i,imo)
     &            -cmoI(iD+1,i,imo)*cmoR(1,i,imo))

c add in a factor of -1/2. The minus sign is cancelled by the
c i*i part (from the prefactor) we ignored above
c An additional factor of -1 is added to correct
c the sign convention. There is a minus sign
c implied in the density matrices for antisymmetric
c properties, but we have to add it explicitly.
        coutR(iD,i)=-0.5d0*coutR(iD,i)
        coutI(iD,i)=0.5d0*coutI(iD,i)

        coutR(4,i) = coutR(4,i)+coutR(iD,i)
        coutI(4,i) = coutI(4,i)+coutI(iD,i)
      end do
      end do

      else
        do j=1,nmo
          if(abs(clincomb(j)).gt.1d-10)then
            do i=1,nbas
            do iD=1,3
c              write(6,*) "clincomb(j): ",clincomb(j)

c See the above note about the sign convention.
c Don't forget we are multiplying by a factor of -i/2
c Left part
              coutI(iD,i)=coutI(iD,i)+0.5d0*clincomb(j)*(
     &                 cmoR(1,i,j)*cmoR(iD+1,i,j)
     &                +cmoI(1,i,j)*cmoI(iD+1,i,j))
              coutR(iD,i)=coutR(iD,i)-0.5d0*clincomb(j)*(
     &                 cmoR(1,i,j)*cmoI(iD+1,i,j)
     &                -cmoI(1,i,j)*cmoR(iD+1,i,j))
c Right part
              coutI(iD,i)=coutI(iD,i)-0.5d0*clincomb(j)*(
     &                 cmoR(iD+1,i,j)*cmoR(1,i,j)
     &                +cmoI(iD+1,i,j)*cmoI(1,i,j))
              coutR(iD,i)=coutR(iD,i)+0.5d0*clincomb(j)*(
     &                 cmoR(iD+1,i,j)*cmoI(1,i,j)
     &                -cmoI(iD+1,i,j)*cmoR(1,i,j))

              coutR(4,i) = coutR(4,i)+coutR(iD,i)
              coutI(4,i) = coutI(4,i)+coutI(iD,i)

            end do
            end do
          endif
        end do ! end loop over mos
      endif
      return
      end

      subroutine derivmo(imo,ipower,ideriv,cmo,clincomb,
     &                   cout,nbas,nmo)
      implicit real*8 (a-h,o-z)
      dimension cmo(4,nbas,nmo),clincomb(nmo),
     &          cout(nbas)
#include "WrkSpc.fh"

      call fzero(cout,nbas)
      write(6,*) "In derivmo"

c 1st element of cmo is the orbital
c 2nd-4th are dx,dy,dz
      iD=ideriv+1

      if(imo.ne.0) then
         do I=1,nbas
           cout(i)=cmo(iD,i,imo)
         end do
         call power(cout,nbas,ipower)
      else
        Call GetMem('TmpMo','Allo','Real',ipTmpMo,nbas)

        do i=1,nmo
          if(clincomb(i).ne.0d0)then
            do j=1,nbas
              work(ipTmpMo-1+j)=cmo(iD,j,i)
            end do
            call power(Work(ipTmpMO),nbas,ipower)
            call daxpy_(nbas,clincomb(i),work(ipTmpMo),1,cout,1)
          endif
        end do

        Call GetMem('TmpMo','Free','Real',ipTmpMo,nbas)

      endif
      return
      end

      subroutine power(c,n,ipower)
      implicit real*8 (a-h,o-z)
      dimension c(n)

      if(ipower.eq.1)then
        return
      elseif(ipower.eq.2)then
        do 100 i=1,n
100     c(i)=c(i)*c(i)
      elseif(ipower.eq.-2)then
        do 200 i=1,n
200     c(i)=sign(c(i)*c(i),c(i))
      endif
      return
      end
      subroutine MakePab(cmo,occ,cout,nCMO,nMOs,nIrrep,nBas)
      implicit real*8 (a-h,o-z)
      dimension cmo(nCMO),occ(nMOs),cout(nMOs), nBas(*)
#include "WrkSpc.fh"
        Call GetMem('List','List','Real',id,id)
        id=0
        id2=0
        call fzero(cout,nMOs)
        do iIrr=0,nIrrep-1
        nd=nBas(iIrr)
        nd2=nd*nd
        do 100 i=1,nd
c        if(occ(i+id).ne.0d0)then
        do 200 j=1,nd
        cout(i+id)=cout(i+id)+occ(i+id)*
     &                ( cmo(j+i*(nd-1)+id2) ** 2 )
200     continue
100     continue
        id=id+nd
        id2=id2+nd2
        enddo

      return
      end
