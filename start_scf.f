      Subroutine Start_Scf(isUHF)
      Implicit None
#include "print.fh"
      Integer iEnd, LuInput, IsFreeUnit, isUHF, ieff_charge
c     Integer iPrint, mtmp
      Character*100 ProgName, Get_ProgName
c      Character*128 FileName
      Character*180 Line
      Character*16 StdIn
      Character*3 cTrap
c     Real*8 Tot_Charge,Tot_El_Charge
      Real*8 Tot_Nuc_Charge
      External IsFreeUnit
*                                                                      *
************************************************************************
*                                                                      *
*     Get the name of the module
*
      ProgName=Get_ProgName()
      Call Upcase(ProgName)
      Call LeftAd(ProgName)
*
      iEnd = 1
 99   If (ProgName(iEnd:iEnd).ne.' ') Then
         iEnd=iEnd+1
         Go To 99
      End If
*                                                                      *
************************************************************************
c      Call GetEnvF('MOLCAS_PRINT',Line)
c      Read(Line,*) iPrint
      Call GetEnvF('MOLCAS_TRAP',Line)
      Read(Line,'(A)') cTrap
      LuInput=11
      LuInput=IsFreeUnit(LuInput)
      Call StdIn_Name(StdIn)
      Call Molcas_Open(LuInput,StdIn)
c      Tot_Charge=0.d0
c      call get_dScalar('Total Charge    ',Tot_Charge)
      Call Get_dScalar('Total Nuclear Charge',Tot_Nuc_Charge)
c      Tot_El_Charge=Tot_Charge-Tot_Nuc_Charge
c      mtmp=Int(-Tot_El_Charge+0.1D0)
*    fix charge if needed
c      ieff_charge=mtmp
c      if(mod(mtmp,2).ne.0) then
c      ieff_charge=mtmp+1
c      endif
      ieff_charge=NInt(Tot_Nuc_Charge)
*
c      Write (LuInput,'(A)') '>> export MOLCAS_PRINT=3'
      if (isUHF.eq.1) then ! SCF module cannot understand UHFORBs as input...
c      Write (LuInput,'(A)') '>>export MOLCAS_TRAP=OFF'
      Write (LuInput,'(A)') '>> COPY INPORB INITORBS'
      Write (LuInput,'(A)') '>> RM INPORB'
      endif

      Write (LuInput,'(A)') ' &Scf'
      Write (LuInput,'(A)') ' Lumorb'
      Write (LuInput,'(A)') ' CHARGE'
      Write (LuInput,  *  )   ieff_charge
      Write (LuInput,'(A)') ' KSDFT'
      Write (LuInput,'(A)') ' B3LYP'
      Write (LuInput,'(A)') ' Iter '
      Write (LuInput,'(A)') ' 0'
c      Write (LuInput,'(A)') ' End of Input'
      Write (LuInput,'(A)') ' '

      if (isUHF.eq.1) then
      Write (LuInput,'(A)') '>> COPY INITORBS INPORB'
      Write (LuInput,'(A)') '>> RM INITORBS'
c      Write (LuInput,'(A,A)') '>>export MOLCAS_TRAP=',cTrap
      endif
c insert here the input of the original Grid_It module:
c      Write (LuInput,'(A,i2)') '>> export MOLCAS_PRINT=',iPrint
      Write (LuInput,'(A)') ' &GRID_IT'
      Write (LuInput,'(A)') ' TOTAL'
      Write (LuInput,'(A)') ' ASCII'
      Write (LuInput,'(A)') ' XFIE'
c      Write (LuInput,'(A)') ' END OF INPUT'
*
      Close(LuInput)
*                                                                      *
************************************************************************
*                                                                      *
      Return
      End
