* $ this file belongs to the Molcas repository $
******************************************************************************
*                                                                            *
* This software is copyrighted. You are granted the use of this software and *
* can redistribute it freely provided that the software is redistributed in  *
* its original form without omissions.                                       *
*                                                                            *
* Any modifications are prohibited unless explicitly permitted by the MOLCAS *
* development group.                                                         *
*                                                                            *
* Copyright: Valera Veryazov 2000-2016                                       *
*           Theoretical Chemistry                                            *
*           Lund University                                                  *
*           Sweden                                                           *
*                                                                            *
******************************************************************************

      Function MyGetKey (InUnit, What, IValue, RValue, SValue,
     &    N, IArray, RArray)
************************************************************
*
*   <DOC>
*     <Name>MyGetKey</Name>
*     <Syntax>MyGetKey(InUnit,What,IValue,RValue,SValue,N,IArray,RArray)</Syntax>
*     <Arguments>
*       \Argument{InUnit}{Unit number}{Integer}{in}
*       \Argument{What}{Type of input}{Character*1}{inout}
*       \Argument{IValue}{Integer Value}{Integer}{out}
*       \Argument{RValue}{Real Value}{Real}{out}
*       \Argument{SValue}{String value}{Character*(*)}{out}
*       \Argument{N}{Size of array}{Integer}{in}
*       \Argument{IArray}{Integer Array}{Integer(N)}{out}
*       \Argument{RArray}{Real Array}{Real(N)}{out}
*     </Arguments>
*     <Purpose> General purpose routine to read arbitrary data from user input
*     </Purpose>
*     <Dependencies></Dependencies>
*     <Author>V.Veryazov</Author>
*     <Modified_by></Modified_by>
*     <Side_Effects></Side_Effects>
*     <Description>
*     The routine read a line from unit InUnit (ignoring molcas comments
*     and blank lines), and return a value or an array.
*
*     Parameter What specifies the type of data:
*     I - read an integer and return it as IValue
*     R - read a real and return it as RValue
*     S - read a string and return it as SValue
*     U - recognize integer/real/string and return corresponding value
*     A - read integer array and return IArray
*     D - read real array and return RArray
*
*     </Description>
*    </DOC>
*
************************************************************
      Implicit Real*8 (A-H,O-Z)
      character SValue *(*)
      Dimension IArray(*), RArray(*)
      character What
      character KWord*120
      MyGetKey=0
      iptr=1
      i=1
 1    Read(InUnit,'(A)',Err=20, end=20) KWord
      If (KWord(1:1).eq.'*'.or.KWord.eq.' ') Go To 1
      Call UpCase(KWord)
        if(What.eq.'I') then
              Read(KWord,*,Err=20) IValue
          else
           if(What.eq.'R') then
               Read(KWord,*,Err=20) RValue
             return
           else
           if(What.eq.'A') then
               Read(KWord,*,Err=20, End=40) (IArray(i),i=iptr,N)
           else

            if(What.eq.'D') then
               Read(KWord,*,Err=20, End=40) (RArray(i),i=iptr,N)
            else

            if(What.eq.'S') then
               Call NoBlanks(SValue, 120, KWord)
               goto 100
            else
             if(What.eq.'U') then
               Read(KWord,*,Err=2) IValue
               What='I'
               Goto 100
2              Read(KWord,*,Err=3) RValue
               What='R'
               Goto 100
3              Call NoBlanks(SValue, 120, KWord)
               What='S'
               Goto 100
             endif
            endif
           endif
          endif
         endif
        endif
100    return
40      iptr=i
        goto 1
20      MyGetKey=1
        return
       end
        subroutine NoBlanks(out,n,in)
        character out*(*), in*(*)
        integer flag
        flag=-1
        j=0
        do 1 i=1,len(in)
         if(flag.eq.-1.and.in(i:i).eq.' ') goto 1
         flag=0
         if(i.le.len(in)-1) then
           if(in(i:i+1).eq.'  ') goto 1
         endif
         j=j+1
         out(j:j)=in(i:i)
1       continue
        out(j+1:)=' '
        return
c Avoid unused argument warnings
        if (.false.) call Unused_integer(n)
        end
