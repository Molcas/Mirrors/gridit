#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <molcastype.h>

#ifdef _CAPITALS_
#define prt_lusc PRT_LUSC
#define lusopen LUSOPEN
#define dump_lusc DUMP_LUSC
#define prgmtranslatec PRGMTRANSLATEC
#else
#ifndef ADD_
#define prt_lusc prt_lusc_
#define lusopen lusopen_
#define dump_lusc dump_lusc_
#define prgmtranslatec prgmtranslatec_
#endif
#endif


int c_write(int *FileDescriptor,char *Buffer,int *nBytes);
void prgmtranslatec (char *, int *, char *, int *, int*);

int prt_lusc(int *lid, char *line, int *len, int *isBin)
{
  int marker[1];
  FILE *fp;
  int i;
  fp = (FILE*) *lid;
  if(*isBin==1){
  marker[0]=*len;
  fwrite(marker,sizeof(int),1, fp);
  }

  for (i = 0; i < *len; i++) fputc(line[i], fp);
  if(*isBin==1){
  marker[0]=*len;
  fwrite(marker,sizeof(int),1, fp);
  }
  else
  {
  fputc(10, fp);
  }
  return 0;
}

int lusopen(int *lid, char *fname, int *fname_len)
{
  FILE *fp;
  char fname_out[1024];
  int tmp0, tmp1;
  int ms = 1;
  fname[*fname_len] = 0;

  tmp0 = strlen(fname);

  prgmtranslatec(fname, &tmp0, fname_out, &tmp1, &ms);

  fp = fopen(fname_out, "wb");
  *lid = (int) fp;
  return 0;
}
void dump_lusc(int *lid, double *Buff, int *nBuff)
{
 FILE *fp;
/* long int d; */
  fp = (FILE*) *lid;
/*  d=ftell(fp); */
  fwrite(Buff,sizeof(double),*nBuff,fp);

 return;
}

